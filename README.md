ToDoList app in React.

- state in Redux
- unit tests with Jest
- animation with framer-motion
- all icons are taken from flaticon.com
- background image is taken from unsplash.com

Add task:

- name your category
- add task to the category
- pick emoji for your task

List of Tasks:

- filter by categories (and All or Show Complete)
- edit task
- sign task as done (only then can you permanently delete the task)
- pin task as important (task is sticky on top of the list)
- num of total / done / each category tasks
- clear the category (remove all tasks from this category)
- clear all categories (remove all tasks)
- warning-modal before remove categories or tasks

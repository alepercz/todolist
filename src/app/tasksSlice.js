import { configureStore, createSlice } from "@reduxjs/toolkit";
import { v4 as uuidv4 } from "uuid";
import { toSentenceCase } from "../utils/formattingString";

const initialState = {
  categories: [],
  tasks: [],
  filterBy: "",
};

const tasksSlice = createSlice({
  name: "todo",
  initialState,
  reducers: {
    addCategory: (state, action) => {
      const newCategory = toSentenceCase(action.payload);

      if (!state.categories.includes(newCategory)) {
        state.categories.push(newCategory);
        state.categories = state.categories.sort();
      }
    },
    removeCategory: (state, action) => {
      state.tasks = state.tasks.filter(
        (task) => task.category !== action.payload
      );
      state.categories = state.categories.filter(
        (category) => category !== action.payload
      );
      if (action.payload === "Show complete")
        state.tasks = state.tasks.filter((task) => task.done === false);
    },
    removeEmptyCategory: (state, action) => {
      const taskWithThisCategoryExist = state.tasks.find(
        (task) => task.category === action.payload
      );
      if (!taskWithThisCategoryExist)
        state.categories = state.categories.filter(
          (category) => category !== action.payload
        );
    },
    addTask: (state, action) => {
      state.tasks.push({
        id: uuidv4(),
        category: toSentenceCase(action.payload.category),
        emoji: action.payload.emoji,
        value: action.payload.input,
        important: false,
        done: false,
      });
    },
    editTask(state, action) {
      state.tasks = state.tasks.filter((task) => {
        if (task.id === action.payload.id) {
          task.value = action.payload.value;
          return task;
        }
        return task;
      });
    },
    removeTaskById: (state, action) => {
      state.tasks = state.tasks.filter((task) => task.id !== action.payload);
    },
    removeAllTasks: (state) => {
      state.categories = [];
      state.tasks = [];
    },
    pinTaskAsImportant: (state, action) => {
      state.tasks = state.tasks.filter((task) => {
        task.id === action.payload.id &&
          (task.important = action.payload.important);
        return task;
      });
    },
    signTaskAsDone: (state, action) => {
      state.tasks = state.tasks.filter((task) => {
        if (task.id === action.payload.id) {
          task.done = action.payload.done;
          task.important = false;
        }
        return task;
      });
    },
    setFilter: (state, action) => {
      state.filterBy = action.payload;
    },
  },
});

export const {
  addCategory,
  removeCategory,
  removeEmptyCategory,
  addTask,
  removeTaskById,
  removeAllTasks,
  editTask,
  pinTaskAsImportant,
  signTaskAsDone,
  setFilter,
} = tasksSlice.actions;

export const store = configureStore({
  reducer: tasksSlice.reducer,
});

export default tasksSlice.reducer;

import reducer, {
  addCategory,
  removeCategory,
  removeEmptyCategory,
  addTask,
  editTask,
  removeTaskById,
  removeAllTasks,
  pinTaskAsImportant,
  signTaskAsDone,
  setFilter,
} from "./tasksSlice";
import { v4 as uuidv4 } from "uuid";
jest.mock("uuid");

test("should return the initial state", () => {
  expect(reducer(undefined, {})).toEqual({
    categories: [],
    tasks: [],
    filterBy: "",
  });
});

describe("categories", () => {
  describe("add category", () => {
    it("should add a category to an inicial list", () => {
      const previousState = {
        categories: [],
        tasks: [],
        filterBy: "",
      };
      expect(reducer(previousState, addCategory("Work"))).toEqual({
        categories: ["Work"],
        tasks: [],
        filterBy: "",
      });
    });
    it("should add a category to an existing list", () => {
      const previousState = {
        categories: ["Work"],
        tasks: [],
        filterBy: "",
      };
      expect(reducer(previousState, addCategory("Home"))).toEqual({
        categories: ["Home", "Work"],
        tasks: [],
        filterBy: "",
      });
    });
    it("should not add the category if this category already exist", () => {
      const previousState = {
        categories: ["Work"],
        tasks: [],
        filterBy: "",
      };
      expect(reducer(previousState, addCategory("Work"))).toEqual({
        categories: ["Work"],
        tasks: [],
        filterBy: "",
      });
    });
  });

  describe("remove category", () => {
    it("should remove an empty category", () => {
      const previousState = {
        categories: ["Home", "Work"],
        tasks: [
          {
            id: "123456789",
            category: "Work",
            emoji: "👩🏻‍💻",
            value: "design a new website",
            important: false,
            done: false,
          },
        ],
        filterBy: "",
      };
      expect(reducer(previousState, removeEmptyCategory("Home"))).toEqual({
        categories: ["Work"],
        tasks: [
          {
            id: "123456789",
            category: "Work",
            emoji: "👩🏻‍💻",
            value: "design a new website",
            important: false,
            done: false,
          },
        ],
        filterBy: "",
      });
    });
    it("should not remove any category because there is no empty category", () => {
      const previousState = {
        categories: ["Home", "Work"],
        tasks: [
          {
            id: "123456789",
            category: "Work",
            emoji: "👩🏻‍💻",
            value: "design a new website",
            important: false,
            done: false,
          },
          {
            id: "987654321",
            category: "Home",
            emoji: "🍲",
            value: "prepare a dinner",
            important: false,
            done: true,
          },
        ],
        filterBy: "",
      };
      expect(reducer(previousState, removeEmptyCategory("Home"))).toEqual({
        categories: ["Home", "Work"],
        tasks: [
          {
            id: "123456789",
            category: "Work",
            emoji: "👩🏻‍💻",
            value: "design a new website",
            important: false,
            done: false,
          },
          {
            id: "987654321",
            category: "Home",
            emoji: "🍲",
            value: "prepare a dinner",
            important: false,
            done: true,
          },
        ],
        filterBy: "",
      });
    });
    it("should remove category", () => {
      const previousState = {
        categories: ["Home", "Work"],
        tasks: [
          {
            id: "123456789",
            category: "Work",
            emoji: "👩🏻‍💻",
            value: "design a new website",
            important: false,
            done: false,
          },
          {
            id: "987654321",
            category: "Home",
            emoji: "🍲",
            value: "prepare a dinner",
            important: false,
            done: false,
          },
          {
            id: "543219876",
            category: "Home",
            emoji: "🧹",
            value: "clean the kitchen",
            important: false,
            done: false,
          },
        ],
        filterBy: "",
      };
      expect(reducer(previousState, removeCategory("Home"))).toEqual({
        categories: ["Work"],
        tasks: [
          {
            id: "123456789",
            category: "Work",
            emoji: "👩🏻‍💻",
            value: "design a new website",
            important: false,
            done: false,
          },
        ],
        filterBy: "",
      });
    });
  });
});

describe("tasks", () => {
  describe("add task", () => {
    it("should add a task to an empty list", () => {
      const previousState = {
        categories: [],
        tasks: [],
        filterBy: "",
      };
      expect(
        reducer(
          previousState,
          addTask({
            input: "design a new website",
            emoji: "👩🏻‍💻",
            category: "work",
          })
        )
      ).toEqual({
        categories: [],
        tasks: [
          {
            id: uuidv4(),
            category: "Work",
            emoji: "👩🏻‍💻",
            value: "design a new website",
            important: false,
            done: false,
          },
        ],
        filterBy: "",
      });
    });
    it("should add a task to an existing list", () => {
      const previousState = {
        categories: [],
        tasks: [
          {
            id: "123456789",
            category: "Work",
            emoji: "👩🏻‍💻",
            value: "design a new website",
            important: false,
            done: false,
          },
        ],
        filterBy: "",
        openWarningModal: false,
        openAddTaskModal: false,
      };
      expect(
        reducer(
          previousState,
          addTask({
            input: "prepare a dinner",
            emoji: "🍲",
            category: "home",
          })
        )
      ).toEqual({
        categories: [],
        tasks: [
          {
            id: "123456789",
            category: "Work",
            emoji: "👩🏻‍💻",
            value: "design a new website",
            important: false,
            done: false,
          },
          {
            id: uuidv4(),
            category: "Home",
            emoji: "🍲",
            value: "prepare a dinner",
            important: false,
            done: false,
          },
        ],
        filterBy: "",
        openWarningModal: false,
        openAddTaskModal: false,
      });
    });
  });

  describe("remove tasks", () => {
    it("should remove a task by ID", () => {
      const previousState = {
        categories: [],
        tasks: [
          {
            id: "123456789",
            category: "Work",
            emoji: "👩🏻‍💻",
            value: "design a new website",
            important: false,
            done: false,
          },
          {
            id: "987654321",
            category: "Home",
            emoji: "🍲",
            value: "prepare a dinner",
            important: false,
            done: false,
          },
        ],
        filterBy: "",
      };
      expect(reducer(previousState, removeTaskById("987654321"))).toEqual({
        categories: [],
        tasks: [
          {
            id: "123456789",
            category: "Work",
            emoji: "👩🏻‍💻",
            value: "design a new website",
            important: false,
            done: false,
          },
        ],
        filterBy: "",
      });
    });
    it("should remove all tasks", () => {
      const previousState = {
        categories: ["Home", "Work"],
        tasks: [
          {
            id: "123456789",
            category: "Work",
            emoji: "👩🏻‍💻",
            value: "design a new website",
            important: false,
            done: false,
          },
          {
            id: "987654321",
            category: "Home",
            emoji: "🍲",
            value: "prepare a dinner",
            important: false,
            done: false,
          },
        ],
        filterBy: "",
      };
      expect(reducer(previousState, removeAllTasks())).toEqual({
        categories: [],
        tasks: [],
        filterBy: "",
      });
    });
  });

  test("should edit the task", () => {
    const previousState = {
      categories: ["Home", "Work"],
      tasks: [
        {
          id: "123456789",
          category: "Work",
          emoji: "👩🏻‍💻",
          value: "design a new website",
          important: false,
          done: false,
        },
        {
          id: "987654321",
          category: "Home",
          emoji: "🍲",
          value: "prepare a dinner",
          important: false,
          done: false,
        },
      ],
      filterBy: "",
    };
    expect(
      reducer(
        previousState,
        editTask({ id: "987654321", value: "prepare a breakfast" })
      )
    ).toEqual({
      categories: ["Home", "Work"],
      tasks: [
        {
          id: "123456789",
          category: "Work",
          emoji: "👩🏻‍💻",
          value: "design a new website",
          important: false,
          done: false,
        },
        {
          id: "987654321",
          category: "Home",
          emoji: "🍲",
          value: "prepare a breakfast",
          important: false,
          done: false,
        },
      ],
      filterBy: "",
    });
  });

  describe("pin task", () => {
    it("should pin the task as important", () => {
      const previousState = {
        categories: ["Home", "Work"],
        tasks: [
          {
            id: "123456789",
            category: "Work",
            emoji: "👩🏻‍💻",
            value: "design a new website",
            important: false,
            done: false,
          },
          {
            id: "987654321",
            category: "Home",
            emoji: "🍲",
            value: "prepare a dinner",
            important: false,
            done: false,
          },
        ],
        filterBy: "",
      };
      expect(
        reducer(
          previousState,
          pinTaskAsImportant({
            id: "987654321",
            important: true,
          })
        )
      ).toEqual({
        categories: ["Home", "Work"],
        tasks: [
          {
            id: "123456789",
            category: "Work",
            emoji: "👩🏻‍💻",
            value: "design a new website",
            important: false,
            done: false,
          },
          {
            id: "987654321",
            category: "Home",
            emoji: "🍲",
            value: "prepare a dinner",
            important: true,
            done: false,
          },
        ],
        filterBy: "",
      });
    });
    it("should unpin the important task", () => {
      const previousState = {
        categories: ["Home", "Work"],
        tasks: [
          {
            id: "123456789",
            category: "Work",
            emoji: "👩🏻‍💻",
            value: "design a new website",
            important: false,
            done: false,
          },
          {
            id: "987654321",
            category: "Home",
            emoji: "🍲",
            value: "prepare a dinner",
            important: true,
            done: false,
          },
        ],
        filterBy: "",
      };
      expect(
        reducer(
          previousState,
          pinTaskAsImportant({
            id: "987654321",
            important: false,
          })
        )
      ).toEqual({
        categories: ["Home", "Work"],
        tasks: [
          {
            id: "123456789",
            category: "Work",
            emoji: "👩🏻‍💻",
            value: "design a new website",
            important: false,
            done: false,
          },
          {
            id: "987654321",
            category: "Home",
            emoji: "🍲",
            value: "prepare a dinner",
            important: false,
            done: false,
          },
        ],
        filterBy: "",
      });
    });
  });

  it("should sign the task as done", () => {
    const previousState = {
      categories: ["Home", "Work"],
      tasks: [
        {
          id: "123456789",
          category: "Work",
          emoji: "👩🏻‍💻",
          value: "design a new website",
          important: false,
          done: false,
        },
        {
          id: "987654321",
          category: "Home",
          emoji: "🍲",
          value: "prepare a dinner",
          important: false,
          done: false,
        },
      ],
      filterBy: "",
    };
    expect(
      reducer(
        previousState,
        signTaskAsDone({
          id: "987654321",
          done: true,
        })
      )
    ).toEqual({
      categories: ["Home", "Work"],
      tasks: [
        {
          id: "123456789",
          category: "Work",
          emoji: "👩🏻‍💻",
          value: "design a new website",
          important: false,
          done: false,
        },
        {
          id: "987654321",
          category: "Home",
          emoji: "🍲",
          value: "prepare a dinner",
          important: false,
          done: true,
        },
      ],
      filterBy: "",
    });
  });
});

it("should set filter to `Work`", () => {
  const previousState = {
    categories: [],
    tasks: [],
    filterBy: "",
  };
  expect(reducer(previousState, setFilter("Work"))).toEqual({
    categories: [],
    tasks: [],
    filterBy: "Work",
  });
});

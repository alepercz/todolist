import React, { useState } from "react";
import { useSelector } from "react-redux";
import WarningModal from "./components/WarningModal/WarningModal";
import AddTaskModal from "./components/AddTaskModal/AddTaskModal";
import CurrentDate from "./components/CurrentDate/CurrentDate";
import TodoList from "./components/TodoList/TodoList";
import Footer from "./components/Footer/Footer";
import { MainButton } from "./components/Buttons/Buttons";
import "./App.css";

const App = () => {
  const [openAddTaskModal, setOpenAddTaskModal] = useState(false);
  const [openWarningModal, setOpenWarningModal] = useState(false);
  const tasks = useSelector((state) => state.tasks);

  return (
    <div className="app" data-testid="app">
      <WarningModal
        openWarningModal={openWarningModal}
        setOpenWarningModal={setOpenWarningModal}
      />
      <AddTaskModal
        openAddTaskModal={openAddTaskModal}
        setOpenAddTaskModal={setOpenAddTaskModal}
      />
      <div>
        <CurrentDate />
        {tasks.length ? (
          <TodoList
            setOpenWarningModal={setOpenWarningModal}
            setOpenAddTaskModal={setOpenAddTaskModal}
          />
        ) : null}
      </div>
      {!tasks.length && (
        <div className="mainButton">
          <MainButton
            onClick={() => setOpenAddTaskModal(true)}
            name="Add new task"
          />
        </div>
      )}
      <Footer />
    </div>
  );
};

export default App;

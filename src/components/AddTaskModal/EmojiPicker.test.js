import "@testing-library/jest-dom/extend-expect";
import { render, cleanup } from "../../test-utils";
import configureStore from "redux-mock-store";
import { Provider } from "react-redux";
import EmojiPicker from "./EmojiPicker";

const mockStore = configureStore([]);

const testStore = mockStore({
  categories: [],
  tasks: [],
  filterBy: "",
});

const renderTestedComponent = (
  component = <EmojiPicker />,
  store = testStore
) => {
  return render(<Provider store={store}>{component}</Provider>);
};

afterEach(cleanup);

it("renders the EmojiPicker component", () => {
  const { getByTestId } = renderTestedComponent();
  expect(getByTestId("emojiPicker")).toBeInTheDocument();
});

it("renders the EmojiPicker and display chosen emoji correctly", () => {
  const { getByTestId } = renderTestedComponent(<EmojiPicker emoji="😍" />);
  expect(getByTestId("emojiPicker")).toHaveTextContent("Your choice: 😍");
});

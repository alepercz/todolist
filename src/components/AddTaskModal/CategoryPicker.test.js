import "@testing-library/jest-dom/extend-expect";
import { render, fireEvent, cleanup } from "../../test-utils";
import configureStore from "redux-mock-store";
import { Provider } from "react-redux";
import CategoryPicker from "./CategoryPicker";

const mockStore = configureStore([]);

const testStore = mockStore({
  categories: [],
  tasks: [],
  filterBy: "",
});

const renderTestedComponent = (
  component = <CategoryPicker />,
  store = testStore
) => {
  return render(<Provider store={store}>{component}</Provider>);
};

afterEach(cleanup);

it("renders the CategoryPicker component", () => {
  const { getByTestId } = renderTestedComponent();
  expect(getByTestId("categoryPicker")).toBeInTheDocument();
});

it("renders Dropdown component", () => {
  const { getByTestId, queryByTestId } = renderTestedComponent(
    <CategoryPicker isCreatingCategory={false} />
  );
  expect(queryByTestId("newCategoryInput")).toBeNull();
  expect(getByTestId("categoryPicker")).toHaveTextContent("Choose category");
});

it("renders input for new category", () => {
  const { getByTestId, getByPlaceholderText } = renderTestedComponent(
    <CategoryPicker isCreatingCategory={true} />
  );
  expect(getByTestId("newCategoryInput")).toBeInTheDocument();
  expect(getByPlaceholderText("New category")).toBeInTheDocument();
  expect(getByTestId("categoryPicker")).not.toHaveTextContent(
    "Choose category"
  );
});

it("renders input and fill with new value", () => {
  const { getByTestId } = renderTestedComponent(
    <CategoryPicker isCreatingCategory={true} />
  );
  const input = getByTestId("newCategoryInput");
  expect(input.value).toBe("");
  fireEvent.change(input, { target: { value: "Work" } });
  expect(input.value).toBe("Work");
});

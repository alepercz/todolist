import "./EmojiPicker.css";
import "emoji-mart/css/emoji-mart.css";
import { Picker } from "emoji-mart";

const EmojiPicker = ({ emoji, onSelect }) => {
  return (
    <div className="pickEmoji" data-testid="emojiPicker">
      <div className="emojiOutput">
        {emoji ? (
          <div className="picked">
            Your choice: <span>{emoji}</span>
          </div>
        ) : (
          "Pick emoji"
        )}
      </div>
      <Picker onSelect={onSelect} />
    </div>
  );
};

export default EmojiPicker;

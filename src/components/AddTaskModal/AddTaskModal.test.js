import "@testing-library/jest-dom/extend-expect";
import { render, fireEvent, cleanup } from "../../test-utils";
import configureStore from "redux-mock-store";
import { Provider } from "react-redux";
import AddTaskModal from "./AddTaskModal";

const mockStore = configureStore([]);

const testStore = mockStore({
  categories: [],
  tasks: [],
  filterBy: "",
});

const renderTestedComponent = (
  component = <AddTaskModal openAddTaskModal={true} />,
  store = testStore
) => {
  return render(<Provider store={store}>{component}</Provider>);
};

afterEach(cleanup);

describe("display component and input without errors", () => {
  it("renders AddTaskModal component", () => {
    const { getByTestId } = renderTestedComponent();
    expect(getByTestId("addTaskModal")).toBeInTheDocument();
  });
  it("renders an input", () => {
    const { getByTestId } = renderTestedComponent();
    expect(getByTestId("taskInput")).toBeInTheDocument();
  });
});

it("renders an input and fill input with value", () => {
  const { getByTestId } = renderTestedComponent();
  const input = getByTestId("taskInput");
  expect(input).toBeInTheDocument();
  fireEvent.change(input, { target: { value: "create a website" } });
  expect(input.value).toBe("create a website");
});

import { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import { addCategory, addTask } from "../../app/tasksSlice.js";
import CategoryPicker from "./CategoryPicker";
import EmojiPicker from "./EmojiPicker";
import { AddButton, CancelButton } from "../Buttons/Buttons.js";
import { motion, AnimatePresence } from "framer-motion";
import "./AddTaskModal.css";

const AddTaskModal = ({ openAddTaskModal, setOpenAddTaskModal }) => {
  const dispatch = useDispatch();
  const [category, setCategory] = useState("");
  const [isCreatingCategory, setIsCreatingCategory] = useState(false);
  const [input, setInput] = useState("");
  const [emoji, setEmoji] = useState("");

  useEffect(() => {
    if (category === "New category") {
      setCategory("");
      setIsCreatingCategory(true);
    }
  }, [category]);

  const addTaskHandler = () => {
    if (category && input && emoji) {
      dispatch(addTask({ category, emoji, input }));
      dispatch(addCategory(category));
      setOpenAddTaskModal(false);
      setInput("");
      setCategory("");
      setEmoji("");
      setIsCreatingCategory(false);
    }
  };

  const categoryHandleChange = (event) => {
    if (event.value) {
      setCategory(event.value);
    } else if (event.target.value || event.target.value === "") {
      setCategory(event.target.value);
    }
  };

  const showCategoriesHandler = () => {
    setCategory("");
    setIsCreatingCategory(false);
  };

  const inputHandleChange = (event) => {
    setInput(event.target.value);
  };

  const handleKeyPress = (event) => {
    if (event.code === "Enter") {
      addTaskHandler();
    }
  };
  const backdropVariants = {
    hidden: { opacity: 0 },
    visible: { opacity: 1 },
  };

  const modalVariants = {
    hidden: { opacity: 0 },
    visible: { opacity: 1, transition: { delay: 0.5 } },
  };

  return (
    <AnimatePresence exitBeforeEnter>
      {openAddTaskModal && (
        <motion.div
          className="addTaskModalBackdrop"
          variants={backdropVariants}
          initial="hidden"
          animate="visible"
          exit="hidden"
          data-testid="addTaskModal"
        >
          <motion.div className="addTaskModalContent" variants={modalVariants}>
            <div className="addTaskModal">
              <h1>New Task</h1>
              <hr className="horizonLineDarkPurple" />
              <CategoryPicker
                category={category}
                isCreatingCategory={isCreatingCategory}
                categoryHandleChange={categoryHandleChange}
                showCategoriesHandler={showCategoriesHandler}
              />
              <div className="taskInput">
                <input
                  type="text"
                  placeholder="What do you want to do?"
                  value={input}
                  onChange={inputHandleChange}
                  onKeyPress={handleKeyPress}
                  data-testid="taskInput"
                />
              </div>
              <EmojiPicker
                emoji={emoji}
                onSelect={(emojiObject) => setEmoji(emojiObject.native)}
                data-testid="emojiPicker"
              />
              <div className="actions">
                <AddButton
                  isActive={category && input && emoji}
                  onClick={addTaskHandler}
                  data-testid="addButton"
                />
                <CancelButton
                  onClick={() => setOpenAddTaskModal(false)}
                  data-testid="cancelButton"
                />
              </div>
            </div>
          </motion.div>
        </motion.div>
      )}
    </AnimatePresence>
  );
};

export default AddTaskModal;

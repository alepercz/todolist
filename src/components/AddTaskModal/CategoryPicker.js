import { useSelector } from "react-redux";
import Dropdown from "react-dropdown";
import { BackButton } from "../Buttons/Buttons.js";
import "./CategoryPicker.css";

const CategoryPicker = (props) => {
  const {
    category,
    isCreatingCategory,
    categoryHandleChange,
    showCategoriesHandler,
  } = props;
  const categories = useSelector((state) => state.categories);
  const options = ["New category"].concat([...categories]);

  return (
    <div className="category" data-testid="categoryPicker">
      {isCreatingCategory ? (
        <div className="ownCategory">
          <input
            type="text"
            placeholder="New category"
            value={category}
            onChange={categoryHandleChange}
            data-testid="newCategoryInput"
          />
          <BackButton onClick={showCategoriesHandler} />
        </div>
      ) : (
        <Dropdown
          options={options}
          onChange={categoryHandleChange}
          value={category}
          placeholder="Choose category"
          className="categoryRoot"
          controlClassName="categoryControl"
          placeholderClassName="categoryPlaceholder"
          menuClassName="categoryMenu"
          arrowClassName="categoryArrow"
        />
      )}
    </div>
  );
};

export default CategoryPicker;

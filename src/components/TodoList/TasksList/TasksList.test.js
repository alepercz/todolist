import "@testing-library/jest-dom/extend-expect";
import { render, cleanup } from "../../../test-utils";
import configureStore from "redux-mock-store";
import { Provider } from "react-redux";
import TasksList from "./TasksList";

const mockStore = configureStore([]);

const testStore = mockStore({
  categories: [],
  tasks: [
    {
      id: "123456789",
      category: "Home",
      emoji: "👩🏻‍💻",
      value: "clean the kitchen",
      important: false,
      done: false,
    },
  ],
  filterBy: "",
});

const renderTestedComponent = (
  component = <TasksList />,
  store = testStore
) => {
  return render(<Provider store={store}>{component}</Provider>);
};

afterEach(cleanup);

it("renders TasksList component", () => {
  const { getByTestId } = renderTestedComponent();
  expect(getByTestId("listOfTasksTitle")).toBeInTheDocument();
  expect(getByTestId("listOfTasks")).toBeInTheDocument();
});

it("renders TasksList component with correct title for filterBy !== 'Show complete'", () => {
  const { getByTestId } = renderTestedComponent();
  expect(getByTestId("listOfTasksTitle")).toBeInTheDocument();
  expect(getByTestId("listOfTasksTitle")).toHaveTextContent("To do");
});

it("renders TasksList component with correct title for filterBy === 'Show complete'", () => {
  const testStore = mockStore({
    categories: [],
    tasks: [
      {
        id: "123456789",
        category: "Home",
        emoji: "👩🏻‍💻",
        value: "clean the kitchen",
        important: false,
        done: false,
      },
      {
        id: "987654321",
        category: "Work",
        emoji: "👩🏻‍💻",
        value: "design a new website",
        important: false,
        done: false,
      },
      {
        id: "678912345",
        category: "Home",
        emoji: "👩🏻‍💻",
        value: "prepare a dinner",
        important: false,
        done: false,
      },
    ],
    filterBy: "Show complete",
  });
  const { getByTestId } = renderTestedComponent(<TasksList />, testStore);
  expect(getByTestId("listOfTasksTitle")).toBeInTheDocument();
  expect(getByTestId("listOfTasksTitle")).toHaveTextContent("Complete");
});

it("renders TasksList component with filterBy === 'Show complete' and correct number of doneTasks", () => {
  const testStore = mockStore({
    categories: [],
    tasks: [
      {
        id: "123456789",
        category: "Home",
        emoji: "👩🏻‍💻",
        value: "clean the kitchen",
        important: false,
        done: true,
      },
      {
        id: "987654321",
        category: "Work",
        emoji: "👩🏻‍💻",
        value: "design a new website",
        important: false,
        done: true,
      },
      {
        id: "678912345",
        category: "School",
        emoji: "👩🏻‍💻",
        value: "homework",
        important: false,
        done: false,
      },
    ],
    filterBy: "Show complete",
  });
  const { getByTestId, queryAllByTestId } = renderTestedComponent(
    <TasksList />,
    testStore
  );
  expect(getByTestId("listOfTasks")).toBeInTheDocument();
  expect(queryAllByTestId("doneTask").length).toBe(2);
});

it("renders TasksList component with filterBy === '' and correct number of allTasks", () => {
  const { getByTestId, queryAllByTestId } = renderTestedComponent();
  expect(getByTestId("listOfTasks")).toBeInTheDocument();
  expect(queryAllByTestId("allTasks").length).toBe(1);
});

it("renders TasksList component with filterBy === 'Home' and correct number of filteredTasks", () => {
  const testStore = mockStore({
    categories: [],
    tasks: [
      {
        id: "123456789",
        category: "Home",
        emoji: "👩🏻‍💻",
        value: "clean the kitchen",
        important: false,
        done: false,
      },
      {
        id: "987654321",
        category: "Work",
        emoji: "👩🏻‍💻",
        value: "design a new website",
        important: false,
        done: false,
      },
      {
        id: "678912345",
        category: "Home",
        emoji: "👩🏻‍💻",
        value: "cook a dinner",
        important: false,
        done: false,
      },
    ],
    filterBy: "Home",
  });
  const { getByTestId, queryAllByTestId } = renderTestedComponent(
    <TasksList />,
    testStore
  );
  expect(getByTestId("listOfTasks")).toBeInTheDocument();
  expect(queryAllByTestId("filteredTask").length).toBe(2);
});

it("renders TasksList with correct message when filterBy === 'Home' and task signed as important", () => {
  const testStore = mockStore({
    categories: [],
    tasks: [
      {
        id: "123456789",
        category: "Home",
        emoji: "👩🏻‍💻",
        value: "clean the kitchen",
        important: true,
        done: false,
      },
    ],
    filterBy: "Home",
  });
  const { getByTestId } = renderTestedComponent(<TasksList />, testStore);
  expect(getByTestId("listOfTasks")).toBeInTheDocument();
  expect(getByTestId("noTaskToDo")).toBeInTheDocument();
});

it("renders TasksList with correct message when there is no ordinary tasks to do because tasks signed as important", () => {
  const testStore = mockStore({
    categories: [],
    tasks: [
      {
        id: "123456789",
        category: "Home",
        emoji: "👩🏻‍💻",
        value: "clean the kitchen",
        important: true,
        done: false,
      },
    ],
    filterBy: "",
  });
  const { getByTestId } = renderTestedComponent(<TasksList />, testStore);
  expect(getByTestId("listOfTasks")).toBeInTheDocument();
  expect(getByTestId("noTaskToDo")).toBeInTheDocument();
});

import { useSelector } from "react-redux";
import Task from "../Task/Task";
import "./TasksList.css";

const TasksList = () => {
  const tasks = useSelector((state) => state.tasks);
  const filterBy = useSelector((state) => state.filterBy);

  const ordinaryTasks = tasks
    .filter((task) => task.important === false)
    .filter((task) => task.done === false);
  const tasksDone = tasks.filter((task) => task.done === true);
  const tasksByCategory = ordinaryTasks.filter(
    (task) => task.category === filterBy
  );
  return (
    <>
      <p className="groupTitle" data-testid="listOfTasksTitle">
        {filterBy === "Show complete" ? "Complete" : "To do"}
      </p>
      <div className="taskList" data-testid="listOfTasks">
        {filterBy === "Show complete" ? (
          tasksDone.map((task) => (
            <Task key={task.id} task={task} dataTestid="doneTask" />
          ))
        ) : filterBy === "" ? (
          ordinaryTasks.length !== 0 ? (
            ordinaryTasks.map((task) => (
              <Task key={task.id} task={task} dataTestid="allTasks" />
            ))
          ) : (
            <p className="noTaskTodo neonTextOrange" data-testid="noTaskToDo">
              No task to do
            </p>
          )
        ) : tasksByCategory.length !== 0 ? (
          tasksByCategory.map((task) => (
            <Task key={task.id} task={task} dataTestid="filteredTask" />
          ))
        ) : (
          <p className="noTaskTodo neonTextOrange" data-testid="noTaskToDo">
            No task to do
          </p>
        )}
      </div>
    </>
  );
};

export default TasksList;

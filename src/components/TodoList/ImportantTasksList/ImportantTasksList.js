import { useSelector } from "react-redux";
import Task from "../Task/Task";
import "./ImportantTasksList.css";

const ImportantTasksList = () => {
  const tasks = useSelector((state) => state.tasks);
  const importantTasks = tasks.filter((task) => task.important === true);

  return (
    importantTasks.length !== 0 && (
      <>
        <p className="groupTitle">Important</p>
        <div className="importantTasksList" data-testid="importantTasksList">
          {importantTasks.map((task) => (
            <Task key={task.id} task={task} dataTestid="importantTask" />
          ))}
        </div>
      </>
    )
  );
};

export default ImportantTasksList;

import "@testing-library/jest-dom/extend-expect";
import { render, cleanup } from "../../../test-utils";
import configureStore from "redux-mock-store";
import { Provider } from "react-redux";
import ImpoortantTasksList from "./ImportantTasksList";

const mockStore = configureStore([]);

const testStore = mockStore({
  categories: [],
  tasks: [
    {
      id: "123456789",
      category: "Home",
      emoji: "👩🏻‍💻",
      value: "clean the kitchen",
      important: false,
      done: false,
    },
    {
      id: "987654321",
      category: "Work",
      emoji: "👩🏻‍💻",
      value: "design a new website",
      important: true,
      done: false,
    },
    {
      id: "678912345",
      category: "School",
      emoji: "👩🏻‍💻",
      value: "homework",
      important: true,
      done: false,
    },
  ],
  filterBy: "",
});

const renderTestedComponent = (
  component = <ImpoortantTasksList />,
  store = testStore
) => {
  return render(<Provider store={store}>{component}</Provider>);
};

afterEach(cleanup);

it("renders ImportantTasksList component", () => {
  const { getByTestId } = renderTestedComponent();
  expect(getByTestId("importantTasksList")).toBeInTheDocument();
});

it("renders ImportantTasksList component with 2 important tasks", () => {
  const { getByTestId, queryAllByTestId } = renderTestedComponent();
  expect(getByTestId("importantTasksList")).toBeInTheDocument();
  expect(queryAllByTestId("importantTask").length).toBe(2);
});

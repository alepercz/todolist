import { useSelector } from "react-redux";
import Task from "../Task/Task";
import "./DoneTasksList.css";

const DoneTasksList = () => {
  const tasks = useSelector((state) => state.tasks);
  const filterBy = useSelector((state) => state.filterBy);

  const tasksDone = tasks.filter((task) => task.done === true);
  const tasksDoneByCategory = tasks
    .filter((task) => task.done === true)
    .filter((task) => task.category === filterBy);

  return (
    ((filterBy === "" && tasksDone.length !== 0) ||
      (filterBy !== "" && tasksDoneByCategory.length !== 0)) &&
    filterBy !== "Show complete" && (
      <>
        <p className="groupTitle" data-testid="doneTasksListTitle">
          Done
        </p>
        <div className="doneTasksList" data-testid="doneTasksList">
          {filterBy === ""
            ? tasksDone.map((task) => (
                <Task key={task.id} task={task} dataTestid="doneTask" />
              ))
            : tasksDoneByCategory.map((task) => (
                <Task key={task.id} task={task} dataTestid="doneCategoryTask" />
              ))}
        </div>
      </>
    )
  );
};

export default DoneTasksList;

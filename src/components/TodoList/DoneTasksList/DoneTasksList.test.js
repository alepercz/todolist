import "@testing-library/jest-dom/extend-expect";
import { render, cleanup } from "../../../test-utils";
import configureStore from "redux-mock-store";
import { Provider } from "react-redux";
import DoneTasksList from "./DoneTasksList";

const mockStore = configureStore([]);

const testStore = mockStore({
  categories: [],
  tasks: [
    {
      id: "123456789",
      category: "Home",
      emoji: "👩🏻‍💻",
      value: "clean the kitchen",
      important: false,
      done: false,
    },
    {
      id: "987654321",
      category: "Work",
      emoji: "👩🏻‍💻",
      value: "design a new website",
      important: true,
      done: true,
    },
  ],
  filterBy: "",
});

const renderTestedComponent = (
  component = <DoneTasksList />,
  store = testStore
) => {
  return render(<Provider store={store}>{component}</Provider>);
};

afterEach(cleanup);

it("renders DoneTasksList component", () => {
  const { getByTestId } = renderTestedComponent();
  expect(getByTestId("doneTasksListTitle")).toBeInTheDocument();
  expect(getByTestId("doneTasksList")).toBeInTheDocument();
});

it("renders DoneTasksList component with doneTasks when filterBy === ''", () => {
  const { getByTestId, queryAllByTestId } = renderTestedComponent();
  expect(getByTestId("doneTasksList")).toBeInTheDocument();
  expect(queryAllByTestId("doneTask").length).toBe(1);
});

it("renders DoneTasksList component with doneTasks when filterBy === ''", () => {
  const testStore = mockStore({
    categories: [],
    tasks: [
      {
        id: "123456789",
        category: "Home",
        emoji: "👩🏻‍💻",
        value: "clean the kitchen",
        important: false,
        done: false,
      },
      {
        id: "987654321",
        category: "Work",
        emoji: "👩🏻‍💻",
        value: "design a new website",
        important: true,
        done: true,
      },
    ],
    filterBy: "Work",
  });
  const { getByTestId, queryAllByTestId } = renderTestedComponent(
    <DoneTasksList />,
    testStore
  );
  expect(getByTestId("doneTasksList")).toBeInTheDocument();
  expect(queryAllByTestId("doneCategoryTask").length).toBe(1);
});

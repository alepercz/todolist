import { useSelector } from "react-redux";
import TasksList from "./TasksList/TasksList.js";
import ImportantTasksList from "./ImportantTasksList/ImportantTasksList.js";
import Statistics from "./Statistics/Statistics";
import DoneTasksList from "./DoneTasksList/DoneTasksList";
import { AddButton, ClearButton } from "../Buttons/Buttons";
import "./TodoList.css";

const TodoList = ({ setOpenWarningModal, setOpenAddTaskModal }) => {
  const filterBy = useSelector((state) => state.filterBy);
  return (
    <div className="todoList" data-testid="todoList">
      <Statistics />
      {filterBy !== "Show complete" && <ImportantTasksList />}
      <TasksList />
      <DoneTasksList />
      <div className="actions neonBorderPurple borderRoundBottom topBorderNone">
        <AddButton
          style={{ minWidth: "160px", padding: "0 8px" }}
          onClick={() => setOpenAddTaskModal(true)}
          name="Add new task"
        />
        <ClearButton
          style={{ minWidth: "160px", padding: "0 8px" }}
          onClick={() => setOpenWarningModal(true)}
          name={`Clear ${filterBy === "" ? "all" : "this"} ${
            filterBy === "" ? "categories" : "category"
          }`}
        />
      </div>
    </div>
  );
};

export default TodoList;

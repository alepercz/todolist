import { useState } from "react";
import { useDispatch } from "react-redux";
import { pinTaskAsImportant } from "../../../app/tasksSlice";
import pinMarkWhite from "../../../icons/pinMarkWhite.png";
import pinMarkBlue from "../../../icons/pinMarkBlue.png";
import unpinMarkWhite from "../../../icons/unpinMarkWhite.png";
import unpinMarkBlue from "../../../icons/unpinMarkBlue.png";

const TaskPinActions = ({ task, isEditing }) => {
  const dispatch = useDispatch();
  const [onHoverPinMark, setOnHoverPinMark] = useState(false);
  const [onHoverUnpinMark, setOnHoverUnpinMark] = useState(false);

  const pinTaskHandler = () => {
    dispatch(pinTaskAsImportant({ id: task.id, important: true }));
  };

  const unpinTaskHandler = () => {
    dispatch(pinTaskAsImportant({ id: task.id, important: false }));
  };

  return task.done ? null : !isEditing && task.important ? (
    <img
      src={onHoverUnpinMark ? unpinMarkBlue : unpinMarkWhite}
      alt="unpin task mark"
      onClick={unpinTaskHandler}
      onMouseEnter={() => setOnHoverUnpinMark(true)}
      onMouseLeave={() => setOnHoverUnpinMark(false)}
    />
  ) : (
    <img
      src={onHoverPinMark ? pinMarkBlue : pinMarkWhite}
      alt="pin task mark"
      onClick={pinTaskHandler}
      onMouseEnter={() => setOnHoverPinMark(true)}
      onMouseLeave={() => setOnHoverPinMark(false)}
    />
  );
};

export default TaskPinActions;

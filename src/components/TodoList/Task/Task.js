import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { editTask } from "../../../app/tasksSlice";
import TaskActions from "./TaskActions";
import TaskPinActions from "./TaskPinActions";
import "./Task.css";

const Task = ({ task, dataTestid = "task" }) => {
  const dispatch = useDispatch();
  const filterBy = useSelector((state) => state.filterBy);
  const [input, setInput] = useState(task.value);
  const [isEditing, setIsEditing] = useState(false);

  const handleChange = (event) => {
    setInput(event.target.value);
  };

  const handleKeyPress = (event) => {
    if (event.code === "Enter") {
      saveTaskHandler();
    }
  };

  const saveTaskHandler = () => {
    const id = task.id;
    const newValue = input;
    dispatch(editTask({ id, value: newValue }));
    setIsEditing(false);
  };

  let taskEditingStyle;
  if (!task.important && !task.done) {
    taskEditingStyle =
      "taskItem neonBorderTaskOrange topBorderNone bottomBorderNone";
  } else {
    taskEditingStyle =
      "taskItem neonBorderTaskPurple topBorderNone bottomBorderNone";
  }

  return (
    <div className="task" data-testid={dataTestid}>
      <div className={taskEditingStyle}>
        <div className="taskAttributes">
          <TaskPinActions task={task} isEditing={isEditing} />
          <div>
            {task.category !== filterBy && (
              <span className="category">{task.category}</span>
            )}
            <span className="emoji">{task.emoji}</span>
          </div>
          {isEditing ? (
            <input
              type="text"
              value={input}
              onChange={handleChange}
              onKeyPress={handleKeyPress}
              data-testid="taskInput"
            />
          ) : (
            <p data-testid="taskValue">{task.value}</p>
          )}
        </div>
        <TaskActions
          task={task}
          editingState={[isEditing, setIsEditing]}
          setInput={setInput}
          saveTaskHandler={saveTaskHandler}
        />
      </div>
    </div>
  );
};

export default Task;

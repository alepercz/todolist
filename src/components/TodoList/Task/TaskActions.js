import { useState } from "react";
import { useDispatch } from "react-redux";

import {
  signTaskAsDone,
  removeEmptyCategory,
  removeTaskById,
} from "../../../app/tasksSlice";

import saveMarkWhite from "../../../icons/saveMarkWhite.png";
import saveMarkGreen from "../../../icons/saveMarkGreen.png";
import cancelMarkWhite from "../../../icons/cancelMarkWhite.png";
import cancelMarkRed from "../../../icons/cancelMarkRed.png";
import checkMarkWhite from "../../../icons/checkMarkWhite.png";
import checkMarkGreen from "../../../icons/checkMarkGreen.png";
import editMarkWhite from "../../../icons/editMarkWhite.png";
import editMarkYellow from "../../../icons/editMarkYellow.png";
import removeMarkWhite from "../../../icons/removeMarkWhite.png";
import removeMarkRed from "../../../icons/removeMarkRed.png";

const TaskActions = (props) => {
  const { task, editingState, setInput, saveTaskHandler } = props;
  const [isEditing, setIsEditing] = editingState;
  const dispatch = useDispatch();

  const [onHoverSaveMark, setOnHoverSaveMark] = useState(false);
  const [onHoverCancelMark, setOnHoverCancelMark] = useState(false);

  const [onHoverCheckMark, setOnHoverCheckMark] = useState(false);
  const [onHoverEditMark, setOnHoverEditMark] = useState(false);
  const [onHoverRemoveMark, setOnHoverRemoveMark] = useState(false);

  const cancelTaskHandler = () => {
    setInput(task.value);
    setIsEditing(false);
  };

  const doneTaskHandler = () => {
    dispatch(signTaskAsDone({ id: task.id, done: true }));
    dispatch(removeEmptyCategory(task.category));
  };

  const editTaskHandler = () => {
    setIsEditing(true);
  };

  const removeTaskHandler = () => {
    dispatch(removeTaskById(task.id));
    dispatch(removeEmptyCategory(task.category));
  };
  return (
    <div className="taskOptions">
      {isEditing ? (
        <>
          <img
            src={onHoverSaveMark ? saveMarkGreen : saveMarkWhite}
            alt="save edited task mark"
            onClick={saveTaskHandler}
            onMouseEnter={() => setOnHoverSaveMark(true)}
            onMouseLeave={() => setOnHoverSaveMark(false)}
            data-testid="saveChanges"
          />
          <img
            src={onHoverCancelMark ? cancelMarkRed : cancelMarkWhite}
            alt="cancel edited task mark"
            onClick={cancelTaskHandler}
            onMouseEnter={() => setOnHoverCancelMark(true)}
            onMouseLeave={() => setOnHoverCancelMark(false)}
          />
        </>
      ) : (
        <>
          {!task.done && (
            <>
              <img
                src={onHoverCheckMark ? checkMarkGreen : checkMarkWhite}
                alt="done task mark"
                onClick={doneTaskHandler}
                onMouseEnter={() => setOnHoverCheckMark(true)}
                onMouseLeave={() => setOnHoverCheckMark(false)}
              />
              <img
                src={onHoverEditMark ? editMarkYellow : editMarkWhite}
                alt="edit task mark"
                onClick={editTaskHandler}
                onMouseEnter={() => setOnHoverEditMark(true)}
                onMouseLeave={() => setOnHoverEditMark(false)}
                data-testid="editTask"
              />
            </>
          )}
          <img
            src={onHoverRemoveMark ? removeMarkRed : removeMarkWhite}
            alt="remove task mark"
            onClick={removeTaskHandler}
            onMouseEnter={() => setOnHoverRemoveMark(true)}
            onMouseLeave={() => setOnHoverRemoveMark(false)}
          />
        </>
      )}
    </div>
  );
};

export default TaskActions;

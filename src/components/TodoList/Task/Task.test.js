import "@testing-library/jest-dom/extend-expect";
import { render, fireEvent, cleanup } from "../../../test-utils";
import configureStore from "redux-mock-store";
import { Provider } from "react-redux";
import Task from "./Task";

const mockStore = configureStore([]);

const testStore = mockStore({
  categories: [],
  tasks: [],
  filterBy: "",
});

const renderTestedComponent = (
  component = (
    <Task task={{ category: "Work", value: "design a website", emoji: "😍" }} />
  ),
  store = testStore
) => {
  return render(<Provider store={store}>{component}</Provider>);
};

afterEach(cleanup);

it("renders Task component", () => {
  const { getByTestId } = renderTestedComponent();
  expect(getByTestId("task")).toBeInTheDocument();
});

it("renders Task component and switch to edit mode", () => {
  const { getByTestId, queryByTestId } = renderTestedComponent();
  expect(getByTestId("editTask")).toBeInTheDocument();
  expect(getByTestId("taskValue")).toBeInTheDocument();
  expect(queryByTestId("taskInput")).toBeNull();
  fireEvent.click(getByTestId("editTask"));
  expect(getByTestId("taskInput")).toBeInTheDocument();
  expect(queryByTestId("taskValue")).toBeNull();
});

it("renders Task component and change task value", () => {
  const { getByTestId } = renderTestedComponent();

  fireEvent.click(getByTestId("editTask"));
  const input = getByTestId("taskInput");
  expect(input.value).toBe("design a website");
  fireEvent.change(input, { target: { value: "cook a dinner" } });
  expect(input.value).toBe("cook a dinner");
});

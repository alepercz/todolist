import "@testing-library/jest-dom/extend-expect";
import { render, cleanup } from "../../../test-utils";
import configureStore from "redux-mock-store";
import { Provider } from "react-redux";
import Statistics from "./Statistics";

const mockStore = configureStore([]);

const testStore = mockStore({
  categories: [],
  tasks: [
    {
      id: "123456789",
      category: "Work",
      emoji: "👩🏻‍💻",
      value: "design a new website",
      important: true,
      done: false,
    },
    {
      id: "987654321",
      category: "Home",
      emoji: "👩🏻‍💻",
      value: "clean the kitchen",
      important: false,
      done: true,
    },
  ],
  filterBy: "",
});

const renderTestedComponent = (
  component = <Statistics />,
  store = testStore
) => {
  return render(<Provider store={store}>{component}</Provider>);
};

afterEach(cleanup);

it("renders Statistics component", () => {
  const { getByTestId } = renderTestedComponent();
  expect(getByTestId("statistics")).toBeInTheDocument();
});

it("renders Statistics component with correct total number of tasks", () => {
  const { getByTestId } = renderTestedComponent();
  const total = getByTestId("totalNumOfTasks");
  expect(total).toBeInTheDocument();
  expect(total).toHaveTextContent("2");
});

it("renders Statistics component with correct total number of tasks", () => {
  const { getByTestId } = renderTestedComponent();
  const total = getByTestId("totalNumOfTasks");
  expect(total).toBeInTheDocument();
  expect(total).toHaveTextContent("2");
});

it("renders Statistics component with correct number of filtered tasks", () => {
  const testStore = mockStore({
    categories: [],
    tasks: [
      {
        id: "123456789",
        category: "Work",
        emoji: "👩🏻‍💻",
        value: "design a new website",
        important: true,
        done: false,
      },
      {
        id: "987654321",
        category: "Home",
        emoji: "👩🏻‍💻",
        value: "clean the kitchen",
        important: false,
        done: true,
      },
    ],
    filterBy: "Work",
  });
  const { getByTestId } = renderTestedComponent(<Statistics />, testStore);
  const filter = getByTestId("filterByCategory");
  expect(filter).toBeInTheDocument();
  expect(filter).toHaveTextContent("1");
});

it("renders Statistics component without number of filtered tasks because filterBy === ''", () => {
  const { queryByTestId } = renderTestedComponent();
  const filter = queryByTestId("numOfFilteredTasks");
  expect(filter).toBeNull();
});

it("renders Statistics component with correct number of done tasks", () => {
  const { getByTestId } = renderTestedComponent();
  const done = getByTestId("numOfDoneTasks");
  expect(done).toBeInTheDocument();
  expect(done).toHaveTextContent("1");
});

import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { setFilter } from "../../../app/tasksSlice.js";
import FilterByCategory from "./FilterByCategory/FilterByCategory.js";
import "./Statistics.css";

const Statistics = () => {
  const dispatch = useDispatch();
  const tasks = useSelector((state) => state.tasks);
  const filterBy = useSelector((state) => state.filterBy);

  const tasksTodo = tasks.filter((task) => task.done === false);
  const tasksDone = tasks.filter((task) => task.done === true);
  const filteredTasks = tasksTodo.filter((task) => task.category === filterBy);

  useEffect(() => {
    if (filterBy === "All") {
      dispatch(setFilter(""));
    }
  }, [dispatch, filterBy]);

  return (
    <div className="statistics" data-testid="statistics">
      <div className="container neonBorderPurple topBorderNone bottomBorderNone">
        <div className="statistic" data-testid="totalNumOfTasks">
          <p>{tasks.length}</p>
          <p>Total</p>
        </div>
        <div className="filter" data-testid="filterByCategory">
          {filterBy === "" || filterBy === "Show complete" ? (
            <br />
          ) : (
            <p data-testid="numOfFilteredTasks">{filteredTasks.length}</p>
          )}
          <FilterByCategory />
        </div>
        <div className="statistic" data-testid="numOfDoneTasks">
          <p>{tasksDone.length}</p>
          <p>Done</p>
        </div>
      </div>
    </div>
  );
};

export default Statistics;

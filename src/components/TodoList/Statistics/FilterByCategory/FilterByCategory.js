import { useDispatch, useSelector } from "react-redux";
import { setFilter } from "../../../../app/tasksSlice";
import Dropdown from "react-dropdown";
import "./FilterByCategory.css";

const FilterByCategory = () => {
  const dispatch = useDispatch();
  const categories = useSelector((state) => state.categories);
  const tasks = useSelector((state) => state.tasks);
  const filterBy = useSelector((state) => state.filterBy);

  const filterOptions = ["All"].concat([...categories]);
  tasks.find((task) => task.done === true) &&
    filterOptions.push("Show complete");
  return (
    <Dropdown
      options={filterOptions}
      onChange={(event) => dispatch(setFilter(event.value))}
      value={filterBy}
      placeholder="Filter ᗊ"
      className="filterRoot"
      controlClassName="filterControl"
      menuClassName="filterMenu"
      arrowClassName="filterArrow"
    />
  );
};

export default FilterByCategory;

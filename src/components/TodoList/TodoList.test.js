import "@testing-library/jest-dom/extend-expect";
import { render, cleanup } from "../../test-utils";
import configureStore from "redux-mock-store";
import { Provider } from "react-redux";
import TodoList from "./TodoList";

const mockStore = configureStore([]);

const testStore = mockStore({
  categories: [],
  tasks: [],
  filterBy: "",
});

const renderTestedComponent = (component = <TodoList />, store = testStore) => {
  return render(<Provider store={store}>{component}</Provider>);
};

afterEach(cleanup);

it("renders TodoList component", () => {
  const { getByTestId } = renderTestedComponent();
  expect(getByTestId("todoList")).toBeInTheDocument();
});

it('renders TodoList component with correct name on clearButton when filterBy is ""', () => {
  const { getByTestId } = renderTestedComponent();
  expect(getByTestId("clearBtn")).toBeInTheDocument();
  expect(getByTestId("clearBtn")).toHaveTextContent("Clear all categories");
});

it('renders TodoList component with correct name on clearButton when filterBy is "Work"', () => {
  const testStore = mockStore({
    categories: [],
    tasks: [],
    filterBy: "Work",
  });
  const { getByTestId } = renderTestedComponent(<TodoList />, testStore);
  expect(getByTestId("clearBtn")).toBeInTheDocument();
  expect(getByTestId("clearBtn")).toHaveTextContent("Clear this category");
});

it("renders TodoList component with ImportantTasksList component because filterBy !== 'Show complete'", () => {
  const testStore = mockStore({
    categories: [],
    tasks: [
      {
        id: "123456789",
        category: "Work",
        emoji: "👩🏻‍💻",
        value: "design a new website",
        important: true,
        done: false,
      },
    ],
    filterBy: "Work",
  });
  const { getByTestId } = renderTestedComponent(<TodoList />, testStore);
  expect(getByTestId("todoList")).toBeInTheDocument();
  expect(getByTestId("importantTasksList")).toBeInTheDocument();
});

it("renders TodoList component without ImportantTasksList component because filterBy === 'Show complete'", () => {
  const testStore = mockStore({
    categories: [],
    tasks: [
      {
        id: "123456789",
        category: "Work",
        emoji: "👩🏻‍💻",
        value: "design a new website",
        important: true,
        done: false,
      },
    ],
    filterBy: "Show complete",
  });
  const { getByTestId, queryByTestId } = renderTestedComponent(
    <TodoList />,
    testStore
  );
  expect(getByTestId("todoList")).toBeInTheDocument();
  expect(queryByTestId("importantTasksList")).toBeNull();
});

import { useSelector } from "react-redux";
import moment from "moment";
import "./CurrentDate.css";

const CurrentDate = () => {
  const tasks = useSelector((state) => state.tasks);
  const currentDay = moment().format("dddd, Do");
  const currentMonth = moment().format("MMMM");

  let styleCurrentDate = "currentDate neonPurple ";

  if (tasks.length) {
    styleCurrentDate += "borderRoundTop bottomBorderNone animateStop";
  } else {
    styleCurrentDate += "borderRoundAll";
  }

  return (
    <div>
      <div className={styleCurrentDate}>
        <h1>{currentDay}</h1>
        <h2>{currentMonth}</h2>
      </div>
    </div>
  );
};

export default CurrentDate;

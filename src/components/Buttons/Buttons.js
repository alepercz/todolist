import { motion } from "framer-motion";
import "./Buttons.css";

const buttonVariants = {
  pulse: {
    scale: 1.1,
    transition: { yoyo: Infinity },
  },
  orangeBg: {
    color: "var(--colorPrimary)",
    backgroundColor: "var(--colorSecondary)",
    borderColor: "var(--colorSecondary)",
  },
  orangeText: {
    color: "var(--colorSecondary)",
  },
  tap: {
    scale: 0.9,
  },
};

const Button = (props) => {
  const {
    style,
    btnClassName,
    onClick,
    initial,
    animate,
    hover,
    name,
    dataTestid,
  } = props;
  let buttonClassName = "btn " + btnClassName;

  return (
    <motion.div
      style={style}
      className={buttonClassName}
      onClick={onClick}
      variants={buttonVariants}
      initial={initial}
      animate={animate}
      whileHover={hover}
      whileTap="tap"
      data-testid={dataTestid}
    >
      {name}
    </motion.div>
  );
};

export const AddButton = (props) => {
  const { style, isActive, onClick, name } = props;

  if (isActive === undefined) {
    return (
      <Button
        style={style}
        btnClassName="active"
        onClick={onClick}
        hover="pulse"
        name={name}
        dataTestid="addBtn"
      />
    );
  }

  return isActive ? (
    <Button
      btnClassName="active"
      onClick={onClick}
      hover="pulse"
      name="+ Add"
      dataTestid="addBtn"
    />
  ) : (
    <Button btnClassName="disabled" name="Add" dataTestid="addBtn" />
  );
};

export const CancelButton = ({ onClick }) => {
  return (
    <Button
      btnClassName="cancel"
      onClick={onClick}
      hover="orangeBg"
      name="Cancel"
      dataTestid="cancelBtn"
    />
  );
};

export const ClearButton = (props) => {
  const { style, onClick, name } = props;
  return (
    <Button
      style={style}
      btnClassName="clear"
      onClick={onClick}
      hover="pulse"
      name={name}
      dataTestid="clearBtn"
    />
  );
};

export const DeleteButton = ({ onClick }) => {
  return (
    <Button
      btnClassName="delete"
      onClick={onClick}
      hover="orangeBg"
      name="Delete"
      dataTestid="deleteBtn"
    />
  );
};

export const BackButton = ({ onClick }) => {
  return (
    <Button
      btnClassName="back"
      onClick={onClick}
      hover="orangeText"
      name="All categories"
      dataTestid="backBtn"
    />
  );
};

export const MainButton = ({ onClick }) => {
  return (
    <Button
      btnClassName="main"
      onClick={onClick}
      hover="pulse"
      name="Add New Task"
      dataTestid="mainBtn"
    />
  );
};

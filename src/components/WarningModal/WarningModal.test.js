import "@testing-library/jest-dom/extend-expect";
import { render, cleanup } from "../../test-utils";
import configureStore from "redux-mock-store";
import { Provider } from "react-redux";
import WarningModal from "./WarningModal";

const mockStore = configureStore([]);

const testStore = mockStore({
  categories: [],
  tasks: [],
  filterBy: "",
});

const renderTestedComponent = (
  component = <WarningModal openWarningModal={true} />,
  store = testStore
) => {
  return render(<Provider store={store}>{component}</Provider>);
};

afterEach(cleanup);

it("renders WarningModal component", () => {
  const { getByTestId } = renderTestedComponent();
  expect(getByTestId("warningModal")).toBeInTheDocument();
});

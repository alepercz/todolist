import { useDispatch, useSelector } from "react-redux";
import {
  removeCategory,
  removeAllTasks,
  setFilter,
} from "../../app/tasksSlice.js";
import { DeleteButton, CancelButton } from "../Buttons/Buttons.js";
import { motion, AnimatePresence } from "framer-motion";
import "./WarningModal.css";

const WarningModal = ({ openWarningModal, setOpenWarningModal }) => {
  const dispatch = useDispatch();
  const filterBy = useSelector((state) => state.filterBy);

  const deleteTasksHandler = () => {
    if (filterBy === "") {
      dispatch(removeAllTasks());
    } else {
      dispatch(removeCategory(filterBy));
      dispatch(setFilter("All"));
    }
    setOpenWarningModal(false);
  };

  const backdropVariants = {
    hidden: { opacity: 0 },
    visible: { opacity: 1 },
  };

  const modalVariants = {
    hidden: { opacity: 0 },
    visible: { opacity: 1, transition: { delay: 0.5 } },
  };

  return (
    <AnimatePresence exitBeforeEnter>
      {openWarningModal && (
        <motion.div
          className="warningModalBackdrop"
          variants={backdropVariants}
          initial="hidden"
          animate="visible"
          exit="hidden"
          data-testid="warningModal"
        >
          <motion.div className="warningModal" variants={modalVariants}>
            <h1>Are you sure?</h1>
            <hr className="horizonLineDarkPurple" />
            <p>Deleted tasks will not be recoverable.</p>
            <div className="actions">
              <DeleteButton onClick={deleteTasksHandler} />
              <CancelButton onClick={() => setOpenWarningModal(false)} />
            </div>
          </motion.div>
        </motion.div>
      )}
    </AnimatePresence>
  );
};

export default WarningModal;

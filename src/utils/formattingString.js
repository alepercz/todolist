export const toSentenceCase = (sentence) => {
  const newSentence = sentence.slice();
  return newSentence[0].toUpperCase() + newSentence.slice(1).toLowerCase();
};

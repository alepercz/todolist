import "@testing-library/jest-dom/extend-expect";
import { render, fireEvent, cleanup, waitForElement } from "./test-utils";
import configureStore from "redux-mock-store";
import { Provider } from "react-redux";
import App from "./App";

const mockStore = configureStore([]);

const testStore = mockStore({
  categories: [],
  tasks: [],
  filterBy: "",
});

const renderTestedComponent = (component = <App />, store = testStore) => {
  return render(<Provider store={store}>{component}</Provider>);
};

afterEach(cleanup);

it("renders App component", () => {
  const { getByTestId } = renderTestedComponent();
  expect(getByTestId("app")).toBeInTheDocument();
});

it("renders App component and open AddTaskModal by click mainButton", async () => {
  const { getByTestId, queryByTestId } = renderTestedComponent();
  const mainBtn = getByTestId("mainBtn");

  expect(mainBtn).toBeInTheDocument();
  expect(queryByTestId("addTaskModal")).toBeNull();
  fireEvent.click(mainBtn);
  const modal = await waitForElement(() => getByTestId("addTaskModal"));
  expect(modal).toBeInTheDocument();
});
